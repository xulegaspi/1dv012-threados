package kernel_src;

import java.util.*;

public class SRTFScheduler extends Thread
{
    private Vector queue; //, testQueue;
    private int timeSlice;
    private static final int DEFAULT_TIME_SLICE = 500;

    // New data added to p161 
    private boolean[] tids; // Indicate which ids have been used
    private static final int DEFAULT_MAX_THREADS = 10000;

    // A new feature added to p161 
    // Allocate an ID array, each element indicating if that id has been used
    private int nextId = 0;
    private void initTid( int maxThreads ) {
      tids = new boolean[maxThreads];
  	  for ( int i = 0; i < maxThreads; i++ )
        tids[i] = false;
    }

    // A new feature added to p161 
    // Search an available thread ID and provide a new thread with this ID
    private int getNewTid( ) {
	    for ( int i = 0; i < tids.length; i++ ) {
  	    int tentative = ( nextId + i ) % tids.length;
  	    if ( tids[tentative] == false ) {
      		tids[tentative] = true;
      		nextId = ( tentative + 1 ) % tids.length;
      		return tentative;
  	    }
	    }
	    return -1;
    }

    // A new feature added to p161 
    // Return the thread ID and set the corresponding tids element to be unused
    private boolean returnTid( int tid ) {
	    if ( tid >= 0 && tid < tids.length && tids[tid] == true ) {
	      tids[tid] = false;
	      return true;
	    }
	    return false;
    }

    // A new feature added to p161 
    // Retrieve the current thread's TCB from the queue
    public TCB getMyTcb( ) {
      Thread myThread = Thread.currentThread( ); // Get my thread object
	    synchronized( queue ) {
  	    for ( int i = 0; i < queue.size( ); i++ ) {
  		    TCB tcb = ( TCB )queue.elementAt( i );
  		    Thread thread = tcb.getThread( );
  		    if ( thread == myThread )             // if this is my TCB, return it
  		      return tcb;
  	    }
  	  }
	    return null;
    }

    // A new feature added to p161 
    // Return the maximum number of threads to be spawned in the system
    public int getMaxThreads( ) {
	    return tids.length;
    }

    public SRTFScheduler() {
	    timeSlice = DEFAULT_TIME_SLICE;
	    queue = new Vector( );
//        testQueue = new Vector( );
	    initTid( DEFAULT_MAX_THREADS );
    }

    public SRTFScheduler(int quantum) {
	    timeSlice = quantum;
	    queue = new Vector( );
//        testQueue = new Vector( );
	    initTid( DEFAULT_MAX_THREADS );
    }

    // A new feature added to p161 
    // A constructor to receive the max number of threads to be spawned
    public SRTFScheduler(int quantum, int maxThreads) {
	    timeSlice = quantum;
	    queue = new Vector( );
//        testQueue = new Vector( );
	    initTid( maxThreads );
    }

    private void schedulerSleep( ) {
	    try {
	      Thread.sleep( timeSlice );
	    } catch ( InterruptedException e ) {
	      }
    }

    private void schedulerSleep( int time ) {
        try {
            Thread.sleep( time );
//            wait(time);
        } catch ( InterruptedException e ) {
        } catch ( Exception e) {

        }
    }

    private void schedulerWakeUp() {
        try {
//            notify();
            //notifyAll();
        } catch (Exception e) {
        }
    }

    // A modified addThread of p161 example
    public TCB addThread( Thread t, int time ) {
	    TCB parentTcb = getMyTcb( );          // get my TCB and find my TID
	    int pid = ( parentTcb != null ) ? parentTcb.getTid( ) : -1;
	    int tid = getNewTid( );               // get a new TID
	    if ( tid == -1)
	      return null;
	    TCB tcb = new TCB( t, tid, pid, time );     // create a new TCB

        // Sorting depending on the remaining time
        synchronized (queue) {
            if (queue.size() > 0) {
                TCB pr = (TCB) queue.firstElement();
//                System.out.println(pr.getThread().getName());
                boolean x = false;
                for (int ii = 0; ii < queue.size(); ii++) {
                    TCB aux = (TCB) queue.get(ii);
//                System.out.println("Comparing threads " + aux.getThread().getName() + " with " + t.getName());
                    if (tcb.getRemTime() < aux.getRemTime() && !x) {
                        if (ii == 0) {
                            pr.getThread().suspend();
                            if (aux.getRemTime() != 0) {
                                System.out.println("Thread out of the core: remaining time = " + (aux.getRemTime() - (new Date().getTime() - aux.getStartTime())));
                                aux.setRemTime(aux.getRemTime() - (new Date().getTime() - aux.getStartTime()));
                                if (aux.getRemTime() < 0) aux.setRemTime(0);
                                queue.add(1, pr);
                            }
                            //queue.add(ii+1, aux);
                            schedulerWakeUp();
                        }
                        queue.add(ii, tcb);
                        x = true;
                        break;
                    }
                }
                if (!x) queue.add(tcb);
            } else queue.add(tcb);
        }
	    return tcb;
    }

    // A new feature added to p161
    // Removing the TCB of a terminating thread
    public boolean deleteThread( ) {
	    TCB tcb = getMyTcb( ); 
	    if ( tcb!= null )
	      return tcb.setTerminated( );
	    else
	      return false;
    }

    public void sleepThread( int milliseconds ) {
	    try {
	      sleep( milliseconds );
	    } catch ( InterruptedException e ) { }
    }
    
    // A modified run of p161
    public void run( ) {
	    Thread current = null;

	    this.setPriority( 6 );
	
	    while ( true ) {
            try {
                // get the next TCB and its thread
                if ( queue.size( ) == 0 )
                  continue;
                TCB currentTCB = (TCB)queue.firstElement( );
                if ( currentTCB.getTerminated( ) ) {
                    queue.remove( currentTCB );
                    returnTid( currentTCB.getTid( ) );
                    continue;
                }
                current = currentTCB.getThread( );
                if ( current != null ) {
                  if ( current.isAlive( ) ) {
                      currentTCB.setStartTime(new Date().getTime());
                      current.resume();
                  } else {
//                      Spawn must be controlled by Scheduler
                      // Scheduler must start a new thread
//                      System.out.println("Thread running: " + current.getName());  // Print
                      currentTCB.setStartTime(new Date().getTime());
                      current.start( );
                  }
                }

                // If the TCB includes a specific remaining time of the process it will be used. If not a time quantum
                long time;
                time = (currentTCB.getRemTime() == 0) ? timeSlice : currentTCB.getRemTime();
//                time = 999999;
//                System.out.println(" ------------------- Scheduler will sleep " + time + " time");  // Print
//                currentTCB.setStartTime(new Date().getTime());
//                System.out.println("Time before sleeping: " + new Date().getTime());
                schedulerSleep((int) time);
//                System.out.println("Time after waking up: " + new Date().getTime());
//                if(time > 0) System.out.println("* * * Context Switch * * * ");  // Print

                synchronized ( queue ) {
                    if ( current != null && current.isAlive( ) ) {
                        if (currentTCB.getRemTime() != 0) {
                            currentTCB.setRemTime(currentTCB.getRemTime() - (new Date().getTime() - currentTCB.getStartTime()));
//                            System.out.println("new RemTime = " + currentTCB.getRemTime());
                        }
//                        current.suspend();
                    }

                    queue.remove( currentTCB ); // rotate this TCB to the end


//                    boolean x = false;
//                    for(int ii=0; ii < queue.size(); ii++) {
//                        TCB aux = (TCB) queue.get(ii);
//                        if(currentTCB.getRemTime() < aux.getRemTime() && !x) {
//                            queue.add(ii, currentTCB);
//                            x = true;
//                            break;
//                        }
//                    }
//                    if(!x) queue.add(currentTCB);

//                    queue.add( currentTCB );
                }
            } catch ( NullPointerException e3 ) { };
	    }
  }
}
