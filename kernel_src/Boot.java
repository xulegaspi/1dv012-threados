package kernel_src;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Boot
{
  static final int OK = 0;
  static final int ERROR = -1;

  public static void main ( String args[] ) {
  	  SysLib.cerr( "threadOS ver 1.0:\n" );
//      SysLib.cerr("Choose Scheduling: ");
      System.out.println("Choose Scheduling: ");
      System.out.println("(1. RR(500), 2. SRTF, 3. MultiLevel, 4. MultiLevel with reset signals");
//      SysLib.cerr("(1. RR(500), 2. SRTF, 3. MultiLevel, 4. MultiLevel with reset signals");
//      String c = System.console().readLine();
      String s = " ";
      try {
          BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
          s = br.readLine();
      } catch (Exception e) {

      }
      System.out.println(Integer.valueOf(s));
  	  SysLib.boot( Integer.valueOf(s) );
      SysLib.cerr( "Type ? for help\n" );

      String[] loader = new String[1];
      loader[0] = "Loader";
      SysLib.exec( loader );
  }
}
