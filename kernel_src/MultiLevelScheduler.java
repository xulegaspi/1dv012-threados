package kernel_src;

import java.util.Date;
import java.util.Vector;

public class MultiLevelScheduler extends Thread
{
    private Vector[] queue;
    private int timeSlice;
    private static final int DEFAULT_TIME_SLICE = 502;

    // New data added to p161
    private boolean[] tids; // Indicate which ids have been used
    private static final int DEFAULT_MAX_THREADS = 10000;

    // A new feature added to p161
    // Allocate an ID array, each element indicating if that id has been used
    private int nextId = 0;
    private void initTid( int maxThreads ) {
      tids = new boolean[maxThreads];
  	  for ( int i = 0; i < maxThreads; i++ )
        tids[i] = false;
    }

    // A new feature added to p161
    // Search an available thread ID and provide a new thread with this ID
    private int getNewTid( ) {
	    for ( int i = 0; i < tids.length; i++ ) {
  	    int tentative = ( nextId + i ) % tids.length;
  	    if ( tids[tentative] == false ) {
      		tids[tentative] = true;
      		nextId = ( tentative + 1 ) % tids.length;
      		return tentative;
  	    }
	    }
	    return -1;
    }

    // A new feature added to p161
    // Return the thread ID and set the corresponding tids element to be unused
    private boolean returnTid( int tid ) {
	    if ( tid >= 0 && tid < tids.length && tids[tid] == true ) {
	      tids[tid] = false;
	      return true;
	    }
	    return false;
    }

    // A new feature added to p161
    // Retrieve the current thread's TCB from the queue
    public TCB getMyTcb( ) {
      Thread myThread = Thread.currentThread( ); // Get my thread object
	    synchronized(queue[0]) {
  	    for ( int i = 0; i < queue[0].size( ); i++ ) {
  		    TCB tcb = ( TCB ) queue[0].elementAt( i );
  		    Thread thread = tcb.getThread( );
  		    if ( thread == myThread )             // if this is my TCB, return it
  		      return tcb;
  	    }
  	  }
	    return null;
    }

    // A new feature added to p161
    // Return the maximum number of threads to be spawned in the system
    public int getMaxThreads( ) {
	    return tids.length;
    }

    public MultiLevelScheduler() {
	    timeSlice = DEFAULT_TIME_SLICE;
        queue = new Vector[3];
	    queue[0] = new Vector();
        queue[1] = new Vector();
        queue[2] = new Vector();
	    initTid( DEFAULT_MAX_THREADS );
    }

    public MultiLevelScheduler(int quantum) {
	    timeSlice = quantum;
        queue = new Vector[3];
        queue[0] = new Vector();
        queue[1] = new Vector();
        queue[2] = new Vector();
	    initTid( DEFAULT_MAX_THREADS );
    }

    // A new feature added to p161
    // A constructor to receive the max number of threads to be spawned
    public MultiLevelScheduler(int quantum, int maxThreads) {
	    timeSlice = quantum;
        queue = new Vector[3];
        queue[0] = new Vector();
        queue[1] = new Vector();
        queue[2] = new Vector();
	    initTid( maxThreads );
    }

    private void schedulerSleep( ) {
	    try {
	      Thread.sleep( timeSlice );
	    } catch ( InterruptedException e ) {
	      }
    }

    private void schedulerSleep( int time ) {
        try {
            Thread.sleep( time );
        } catch ( InterruptedException e ) {
        }
    }

    // A modified addThread of p161 example
    public TCB addThread(Thread t, int time) {
	    TCB parentTcb = getMyTcb( );          // get my TCB and find my TID
	    int pid = ( parentTcb != null ) ? parentTcb.getTid( ) : -1;
	    int tid = getNewTid( );               // get a new TID
	    if ( tid == -1)
	      return null;
	    TCB tcb = new TCB( t, tid, pid, time );     // create a new TCB
        synchronized (queue[0]) {
            queue[0].add(tcb);
        }
	    return tcb;
    }

    // A new feature added to p161
    // Removing the TCB of a terminating thread
    public boolean deleteThread( ) {
	    TCB tcb = getMyTcb( ); 
	    if ( tcb!= null )
	      return tcb.setTerminated( );
	    else
	      return false;
    }

    public void sleepThread( int milliseconds ) {
	    try {
	      sleep( milliseconds );
	    } catch ( InterruptedException e ) { }
    }
    
    // A modified run of p161
    public void run( ) {
	    Thread current = null;
        int queueNum = 0;
	    this.setPriority( 6 );
	
	    while ( true ) {
	    try {
		    // get the next TCB and its thread choosing the queue it belongs to
		    if ( queue[0].size() == 0 ) {
                if ( queue[1].size( ) == 0 ) {
                    if (queue[2].size() == 0 ) {
                        continue;
                    } else queueNum = 2;
                } else queueNum = 1;
            } else queueNum=0;

            //System.out.println("------>Getting thread from queue: " + queueNum);  // Print
		    TCB currentTCB = (TCB) queue[queueNum].firstElement( );

		    if ( currentTCB.getTerminated( ) ) {
                //System.out.println("Thread terminated: " + current.getName() + " ------> State: " + current.getState());  // Print
                queue[queueNum].remove(currentTCB);
                returnTid( currentTCB.getTid( ) );
                continue;
		    }
		    current = currentTCB.getThread( );
		    if ( current != null ) {
		        if ( current.isAlive( ) ) {
                    //System.out.println("Thread resumed: " + current.getName() + " ------> State: " + current.getState());  // Print
                    currentTCB.setStartTime(new Date().getTime());
                    current.resume();
                } else {
                    // Spawn must be controlled by Scheduler
                    // Scheduler must start a new thread
//                    System.out.println("Thread started running: " + current.getName() + " ------> State: " + current.getState());  // Print
                    if (current.getState() == State.TERMINATED) {
//                      System.out.println("QueueNum: " + queueNum);  // Print
                        queue[queueNum].remove(currentTCB);
                    } else {
                        currentTCB.setStartTime(new Date().getTime());
                        current.start();
                    }
		        }
		    }

            int time = 0;
            switch (queueNum) {
                case 0:
                    time = 500;
                    break;
                case 1:
                    time = 1000;
                    break;
                case 2:
                    time = (int)currentTCB.getRemTime();
                    if(time==0) time = 100;
                    break;
            }
		    schedulerSleep( time );
//		    System.out.println("* * * Context Switch * * * ");

		    synchronized (queue) {
                if ( current != null && current.isAlive( ) )
                    current.suspend();
                queue[queueNum].remove(currentTCB); // rotate this TCB to the end
                if (!currentTCB.getTerminated()) {

                    // Decide in which queue the process will be putted
                    switch (queueNum) {
                        case 0:
                            if (currentTCB.getRemTime() >= 500) {
                                currentTCB.setRemTime(currentTCB.getRemTime() - 500);
                                // Error Handling
                                if (currentTCB.getRemTime() == 0 && current != null) {
                                    current.resume();
                                    currentTCB.setTerminated();
                                }
                            } else currentTCB.setRemTime(0);
                            queue[queueNum + 1].add(currentTCB);
                            break;

                        case 1:
                            if (currentTCB.getRemTime() >= 1000) {
                                currentTCB.setRemTime(currentTCB.getRemTime() - 1000);
                                // Error Handling
                                if (currentTCB.getRemTime() == 0 && current != null) {
                                    current.resume();
                                    currentTCB.setTerminated();
                                }
                            } else currentTCB.setRemTime(0);

                            // Sorting processes for the last queue in SRTF
                            if(queue[2].size() > 0) {
                                boolean x = false;
                                for(int ii=0; ii < queue[2].size(); ii++) {
                                    TCB aux = (TCB) queue[2].get(ii);
                                    if(currentTCB.getRemTime() < aux.getRemTime() && !x && currentTCB.getRemTime() != 0) {
                                        if (ii == 0) {
                                            aux.getThread().suspend();
                                            aux.setRemTime(aux.getRemTime() - (new Date().getTime() - aux.getStartTime()));
                                            if (aux.getRemTime() < 0) aux.setRemTime(0);
                                            queue[2].add(1, aux);
                                        }
                                        queue[2].add(ii, currentTCB);
                                        x = true;
                                        break;
                                    }
                                }
                                if(!x) queue[2].add(currentTCB);
                            } else queue[2].add(currentTCB);
                            break;

                        case 2:
                            // Error Handling
                            if (currentTCB.getRemTime() == 0) {
                                current.resume();
                                currentTCB.setTerminated();
                            } else {
                                long time1 = currentTCB.getRemTime();
                                long time2 = currentTCB.getStartTime();
                                long time3 = new Date().getTime();
                                long time4 = time3 - time2;
//                                System.out.println(" --- Time executed: " + time4 + ". Prev time remaining: " + time1);  // Print
                            }
                            queue[2].add(currentTCB);
                            break;
                    }
                } else queue[queueNum].remove(currentTCB);
            }
	    } catch ( NullPointerException e3 ) {
            // Error handling
            e3.printStackTrace();
        };
	  }
  }
}
