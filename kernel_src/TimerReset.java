package kernel_src;

import java.util.Date;

/**
 * Created by Xurxo on 14/01/2015.
 */
public class TimerReset extends Thread {

    private static final int RESETTIME = 5000;

    public TimerReset() {
    }

    private void timerSleep() {
        try {
//            wait(RESETTIME);
            Thread.sleep(RESETTIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void run() {
        long time = new Date().getTime();
        try {
            MultiLevelScheduler2.bTimerReset = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        timerSleep();
//            long time2 = new Date().getTime() - time;
//            System.out.println("Time Slept = " + time2);  // Print
        try {
            Kernel.schedulerML2.reset();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            MultiLevelScheduler2.bTimerReset = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        stop();
    }
}
